El script solo funciona en sistemas basados en debian.

This only works in debian based GNU/Linux distros.

# MODO DE USO:

Antes de empezar deberías tener al menos un sitio web (que es el que vamos a validar)

- Descarga ambos scripts y mételos en la carpeta que quieras.
- Como root ejecuta creaEntidad para crear la Autoridad Certificadora.
- Modifica el archivo openssl.cnf con los datos y rutas de la Autoridad que has creado.
- Ejecuta el archivo validaCert también como root para generar una petición de certificación y firmarla.

# HOW TO:

Before starting you would need at least one web site which we will validate later.

- Download both scripts and place them in your PATH or whatever.
- As root execute creaEntidad to create the Certificate Authority.
- Modify your openssl.cnf in your CA folder with the your CA info and paths.
- Execute validaCert as root to send a certificate request and also signing it.
